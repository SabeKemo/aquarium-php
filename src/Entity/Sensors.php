<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sensors
 *
 * @ORM\Table(name="sensors", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="timestamp_index", columns={"timestamp"})})
 * @ORM\Entity
 */
class Sensors
{
    public function __construct()
    {
        $this->timestamp = new \DateTime('UTC');
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true, options={"default"="current_timestamp()"})
     */
    private $timestamp = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="l_t", type="decimal", precision=5, scale=2, nullable=true, options={"default"="NULL"})
     */
    private $lockerTemp = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="r_t", type="decimal", precision=5, scale=2, nullable=true, options={"default"=NULL})
     */
    private $roomTemp = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="w_t", type="decimal", precision=5, scale=2, nullable=true, options={"default"=NULL})
     */
    private $waterTemp = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tds", type="smallint", nullable=true, options={"default"=NULL,"unsigned"=true})
     */
    private $tds = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="w_l", type="decimal", precision=5, scale=2, nullable=true, options={"default"=NULL})
     */
    private $waterLevel = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ph", type="decimal", precision=4, scale=2, nullable=true, options={"default"=NULL})
     */

    private $ph = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="w_c", type="smallint", nullable=true, options={"default"=NULL,"unsigned"=true})
     */
    private $waterClarity = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="w_f", type="smallint", nullable=true, options={"default"=NULL})
     */
    private $waterFlow = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(?\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getLockerTemp(): ?string
    {
        return $this->lockerTemp;
    }

    public function setLockerTemp(?string $lockerTemp): self
    {
        $this->lockerTemp = $lockerTemp;

        return $this;
    }

    public function getRoomTemp(): ?string
    {
        return $this->roomTemp;
    }

    public function setRoomTemp(?string $roomTemp): self
    {
        $this->roomTemp = $roomTemp;

        return $this;
    }

    public function getWaterTemp(): ?string
    {
        return $this->waterTemp;
    }

    public function setWaterTemp(?string $waterTemp): self
    {
        $this->waterTemp = $waterTemp;

        return $this;
    }

    public function getTds(): ?int
    {
        return $this->tds;
    }

    public function setTds(?int $tds): self
    {
        $this->tds = $tds;

        return $this;
    }

    public function getWaterLevel(): ?string
    {
        return $this->waterLevel;
    }

    public function setWaterLevel(?string $waterLevel): self
    {
        $this->waterLevel = $waterLevel;

        return $this;
    }

    public function getPh(): ?string
    {
        return round($this->ph,2);
    }

    public function setPh(?string $ph): self
    {
        $this->ph = $ph;

        return $this;
    }

    public function getWaterClarity(): ?int
    {
        return $this->waterClarity;
    }

    public function setWaterClarity(?int $waterClarity): self
    {
        $this->waterClarity = $waterClarity;

        return $this;
    }

    public function getWaterFlow(): ?int
    {
        return $this->waterFlow;
    }

    public function setWaterFlow(?int $waterFlow): self
    {
        $this->waterFlow = $waterFlow;

        return $this;
    }
    public function getJSON() {
        $return = new \stdClass();
        $return->l_t = $this->getLockerTemp();
        $return->w_f = $this->getWaterFlow();
        $return->ph = $this->getPh();
        $return->w_c = $this->getWaterClarity();
        $return->w_l = $this->getWaterLevel();
        $return->tds = $this->getTds();
        $return->r_t = $this->getRoomTemp();
	$return->w_t = $this->getWaterTemp();
        return json_encode($return);
    }
}
