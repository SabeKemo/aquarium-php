<?php


namespace App\Services;
use karpy47\PhpMqttClient\MQTTClient;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MQTT
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function send(string $topic, string $message) {
        $mqtt_settings = $this->params->get('mqtt');

        $client = new MQTTClient($mqtt_settings['server'], $mqtt_settings['port']);
        $client->setAuthentication($mqtt_settings['user'],$mqtt_settings['pass']);
        $success = $client->sendConnect($mqtt_settings['client']);  // set your client ID

        if ($success) {
            $client->sendPublish($topic, $message);
            $client->sendDisconnect();
        }
        $client->close();
    }
}