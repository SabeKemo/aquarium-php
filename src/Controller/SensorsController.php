<?php
namespace App\Controller;

use App\Entity\Sensors;
use App\Services\MQTT;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SensorsController extends AbstractController
{
    private $mqtt;
    public function __construct(MQTT $mqtt)
    {
        $this->mqtt = $mqtt;
    }

    /**
    * @Route("/record", name="record")
    */
    public function record(): Response
    {
        $request = Request::createFromGlobals();
        $json = $request->getContent();

        $jsonData = json_decode($json);

        if ($jsonData === null) return new Response();

        $entityManager = $this->getDoctrine()->getManager();

        $sensors = new Sensors();
        foreach($jsonData as $key => $value) {
            if ($key === 'l_t') $sensors->setLockerTemp($value);
	    if ($key === 'w_t') $sensors->setWaterTemp($value);
            if ($key === 'w_f') $sensors->setWaterFlow($value);
            if ($key === 'ph') $sensors->setPh($value);
        }

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($sensors);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        $this->mqtt->send('d/d/a',$sensors->getJSON());

        return new Response();
    }
}
